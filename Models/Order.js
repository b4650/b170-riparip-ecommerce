const mongoose = require("mongoose")

const orderSchema = new mongoose.schema({
		totalAmount:{
		type: Number
	},
		purschasedOn:{
		type: Date,
		default: new Date()
	},
		price:{
		type: Number,
		required: [true, "Price is required"]
	},
		isActive:{
		type: Boolean,
		default: true
	},
		createdOn:{
		type: Date,
		default: new Date()
	}		
})

module.exports = mongoose.model("Order", userSchema)